package com.akounda.universitymanagement_core.model;

import com.akounda.universitymanagement_core.domain.EtudiantAcademie;
import com.akounda.universitymanagement_core.domain.Note;

public class EtudiantNote {

	private EtudiantAcademie etudiant ;
	private Note note ;
	
	public EtudiantAcademie getEtudiantAcademie() {
		return etudiant;
	}
	public void setEtudiantAcademie(EtudiantAcademie etudiantAcademie) {
		this.etudiant = etudiantAcademie;
	}
	public Note getNote() {
		return note;
	}
	public void setNote(Note note) {
		this.note = note;
	}
	
	
}
