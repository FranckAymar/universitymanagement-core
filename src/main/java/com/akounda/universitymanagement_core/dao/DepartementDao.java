package com.akounda.universitymanagement_core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.IDepartementDao;
import com.akounda.universitymanagement_core.domain.Departement;
import com.akounda.universitymanagement_core.domain.Etudiant;

@Repository
public class DepartementDao implements IDepartementDao {
	
	@PersistenceContext
	private EntityManager em ;

	public Departement ajouter(Departement departement) {
		// TODO Auto-generated method stub
		em.persist(departement);
		return departement;
	}

	public List<Departement> recupererDepartement() {
		// TODO Auto-generated method stub
		
		List<Departement> departements;
		Query q;

		q = em.createQuery("select dept from Departement dept");
		departements = q.getResultList();

		return departements;
	}

	public Departement rechercherDepartementParId(Long id) {
		// TODO Auto-generated method stub
		
		Departement departement = em.find(Departement.class, id) ;
		return departement;
	}

	public Departement supprimerDepartement(Long id) {
		// TODO Auto-generated method stub
		Departement departement = rechercherDepartementParId(id) ;
		em.remove(departement);
		return null;
	}

	public Departement modifierDepartement(Departement departement) {
		// TODO Auto-generated method stub
		
		em.merge(departement) ;
		return departement;
	}

}
