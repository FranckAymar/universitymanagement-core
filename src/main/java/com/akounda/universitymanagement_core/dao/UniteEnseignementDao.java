package com.akounda.universitymanagement_core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.IUniteEnseignementDao;
import com.akounda.universitymanagement_core.domain.UniteEnseignement;

@Repository
public class UniteEnseignementDao implements IUniteEnseignementDao {

	@PersistenceContext
	private EntityManager em ;
	
	public UniteEnseignement recupererUE(Long id) {
		// TODO Auto-generated method stub
		UniteEnseignement ue = em.find(UniteEnseignement.class, id) ;
		return ue;
	}

	public UniteEnseignement ajouter(UniteEnseignement ue) {
		// TODO Auto-generated method stub
		em.persist(ue);
		return ue;
	}

	public UniteEnseignement modifier(UniteEnseignement ue) {
		// TODO Auto-generated method stub
		em.merge(ue) ;
		return ue;
	}

	public List<UniteEnseignement> recupererUEs() {
		// TODO Auto-generated method stub
		Query q = em.createQuery("select ue from UniteEnseignement ue") ;
		List<UniteEnseignement> ues = q.getResultList() ;
		return ues;
	}

}
