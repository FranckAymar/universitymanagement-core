package com.akounda.universitymanagement_core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.IProfesseurDao;
import com.akounda.universitymanagement_core.domain.Etudiant;
import com.akounda.universitymanagement_core.domain.Professeur;

@Repository
public class ProfesseurDao implements IProfesseurDao {

	@PersistenceContext
	EntityManager em ;
	
	public Professeur ajouter(Professeur prof) {
		
		if(prof.getId() == null) {
			 em.persist(prof);
			 return prof ;
		}else {
			modifierPro(prof) ;
			return prof ;
		}
		
	}

	public List<Professeur> recupererPro() {
		// TODO Auto-generated method stub
		
		javax.persistence.Query q = em.createQuery("select prof from Professeur prof where prof.actif = true ") ;
		List<Professeur> profs = q.getResultList();
		return profs ;
	}

	public Professeur modifierPro(Professeur prof) {
		// TODO Auto-generated method stub
		em.merge(prof) ;
		return prof;
	}

	public Professeur supprimerPro(Long id) {
		// TODO Auto-generated method stub
		Professeur prof = recupererProParId(id) ;
		prof.setActif(false);
		
		return prof ;
	}

	public Professeur recupererProParId(Long id) {
		// TODO Auto-generated method stub
		Professeur prof = em.find(Professeur.class, id) ;
		
		return prof ;
	}


}
