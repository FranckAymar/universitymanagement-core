package com.akounda.universitymanagement_core.dao.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Etudiant;
import com.akounda.universitymanagement_core.domain.EtudiantAcademie;

public interface IEtudiantDao {

	public Etudiant ajouter(Etudiant etudiant);

	public List<Etudiant> getAllEtudiant() ;
	
	public Etudiant modifier(Etudiant etudiant) ;
	
	public Etudiant supprimer(Long id) ;
	
	public Etudiant rechercherEtudiantParId(Long id) ;

}
