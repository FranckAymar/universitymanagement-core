package com.akounda.universitymanagement_core.dao.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.UniteEnseignement;

public interface IUniteEnseignementDao {

	public UniteEnseignement ajouter(UniteEnseignement ue ) ;
	public List<UniteEnseignement> recupererUEs() ;
	public UniteEnseignement recupererUE(Long id) ;
	public UniteEnseignement modifier(UniteEnseignement ue) ;
}
