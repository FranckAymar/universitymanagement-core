package com.akounda.universitymanagement_core.dao.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Examen;

public interface IExamenDao {
	
	public Examen ajouter(Examen examen) ;
	public List<Examen> recupererExamen() ;
	public Examen modfierExamen(Examen examen) ;
	
}
