package com.akounda.universitymanagement_core.dao.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Annee;

public interface IAnneeDao {

	public Annee ajouter(Annee annee) ;
	public List<Annee> recupererAnnees() ;
	public Annee recupererParId(Long id);
	public Annee modfierAnnee(Annee annee) ;

	
}
