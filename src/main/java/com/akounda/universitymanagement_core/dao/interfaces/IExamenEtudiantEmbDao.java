package com.akounda.universitymanagement_core.dao.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.ExamenEtudiantEmb;

public interface IExamenEtudiantEmbDao {
	
	public ExamenEtudiantEmb ajouter(ExamenEtudiantEmb examenEtudiantEmb) ;
	public List<ExamenEtudiantEmb> recuperer() ;
	public List<ExamenEtudiantEmb> recupererParExamen(Long idExamen) ;

}
