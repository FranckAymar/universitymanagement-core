package com.akounda.universitymanagement_core.dao.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Niveau;

public interface INiveauDao {

	public Niveau ajouter(Niveau niveau) ;
	public List<Niveau> recupererNiveaux() ;
	public Niveau rechercherNiveauParId(Long id);
	public Niveau supprimerNiveau(Long id) ;
	public Niveau modifierNiveau(Niveau niveau) ;
}
