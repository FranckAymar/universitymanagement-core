package com.akounda.universitymanagement_core.dao.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.EtudiantAcademie;

public interface IEtudiantAcademieDao {
	
	
	public EtudiantAcademie ajouter(EtudiantAcademie etudiant);

	public List<EtudiantAcademie> getAllEtudiantAcademie(Long idAnnee) ;

	public List<EtudiantAcademie> recupererParFiliereEtNiveau(Long idFiliere, Long idNiveau) ;
	
	public EtudiantAcademie modifierAcademieEtudiant(EtudiantAcademie etudiant) ;
	
	public EtudiantAcademie supprimerAcademieEtudiant(Long id) ;
	
	public EtudiantAcademie rechercherEtudiantAcademieParId(Long id) ;

}
