package com.akounda.universitymanagement_core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.IFiliereDao;
import com.akounda.universitymanagement_core.domain.Filiere;

@Repository
public class FiliereDao implements IFiliereDao {

	@PersistenceContext
	private EntityManager em ;
	
	public Filiere ajouter(Filiere filiere) {
		// TODO Auto-generated method stub
		em.persist(filiere);
		return filiere;
	}

	public List<Filiere> recupererFiliere() {
		// TODO Auto-generated method stub
		Query q = em.createQuery("select f from Filiere f") ;
		List<Filiere> filieres = q.getResultList() ;
		return filieres;
	}

	public Filiere rechercherFiliereParId(Long id) {
		// TODO Auto-generated method stubf
		Filiere filiere = em.find(Filiere.class, id) ;
		return filiere;
	}

	public Filiere supprimerFiliere(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public Filiere modifierFiliere(Filiere filiere) {
		// TODO Auto-generated method stub
		em.merge(filiere) ;
		return filiere;
	}

}
