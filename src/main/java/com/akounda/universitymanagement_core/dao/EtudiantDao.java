package com.akounda.universitymanagement_core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.IEtudiantDao;
import com.akounda.universitymanagement_core.domain.Etudiant;
import com.akounda.universitymanagement_core.domain.EtudiantAcademie;

@Repository
public class EtudiantDao implements IEtudiantDao {

	@PersistenceContext
	EntityManager em;

	public Etudiant ajouter(Etudiant etudiant) {
		// TODO Auto-generated method stub
		em.persist(etudiant);
		return etudiant;
	}

	// Retourne les etudiants qui ne ssont pas supprimés dans la base de données
	public List<Etudiant> getAllEtudiant() {
		// TODO Auto-generated method stub

		List<Etudiant> etudiants;
		Query q;

		q = em.createQuery("select ets from Etudiant ets where ets.actif=true");
		etudiants = q.getResultList();

		return etudiants;
	}

	public Etudiant modifier(Etudiant etudiant) {
		// TODO Auto-generated method stub

		Etudiant etudiantModif = rechercherEtudiantParId(etudiant.getId());
		etudiantModif = etudiant;
		em.merge(etudiantModif);

		return etudiantModif;

	}

	public Etudiant supprimer(Long id) {
		// TODO Auto-generated method stub

		Etudiant etudiant = rechercherEtudiantParId(id);
		etudiant.setActif(false);
		return etudiant;

	}

	public Etudiant rechercherEtudiantParId(Long id) {
		// TODO Auto-generated method stub

		Etudiant etudiant = em.find(Etudiant.class, id);
		return etudiant;
	}



}
