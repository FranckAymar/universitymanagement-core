package com.akounda.universitymanagement_core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.INiveauDao;
import com.akounda.universitymanagement_core.domain.Niveau;

@Repository
public class NiveauDao implements INiveauDao {

	@PersistenceContext
	private EntityManager em ;
	
	public Niveau ajouter(Niveau niveau) {
		// TODO Auto-generated method stub
		em.persist(niveau);
		return niveau;
	}

	public List<Niveau> recupererNiveaux() {
		// TODO Auto-generated method stub
		
		Query q = em.createQuery("select n from Niveau n") ;
		List<Niveau> niveaux = q.getResultList() ;
		return niveaux;
	}

	public Niveau rechercherNiveauParId(Long id) {
		// TODO Auto-generated method stub
		
		Niveau n = em.find(Niveau.class, id) ;
		return n;
	}

	public Niveau supprimerNiveau(Long id) {
		// TODO Auto-generated method stub
		
		
		return null;
	}

	public Niveau modifierNiveau(Niveau niveau) {
		// TODO Auto-generated method stub
		
		em.merge(niveau) ;
		return niveau;
	}

}
