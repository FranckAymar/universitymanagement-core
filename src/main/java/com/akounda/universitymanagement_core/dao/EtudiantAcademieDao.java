package com.akounda.universitymanagement_core.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.IEtudiantAcademieDao;
import com.akounda.universitymanagement_core.domain.EtudiantAcademie;

@Repository
public class EtudiantAcademieDao implements IEtudiantAcademieDao{
	
	@PersistenceContext
	EntityManager em;
	
	public List<EtudiantAcademie> getAllEtudiantAcademie(Long idAnnee) {
		// TODO Auto-generated method stub
		List<EtudiantAcademie> etudiants ;
		Query q ;
		if(idAnnee==null ) {
			 q = em.createQuery("select ets from EtudiantAcademie ets where ets.isActif=true");
			 etudiants = q.getResultList() ;
		 }else {
			  q = em.createQuery("select ets from EtudiantAcademie ets where ets.annee.id=?1 and ets.isActif=true") ;
			  q.setParameter(1, idAnnee) ;
			 // q.setParameter(2, idNiveau) ;
			  etudiants = q.getResultList() ;  
		 }
		
		return etudiants ;
	}
	
	public List<EtudiantAcademie> recupererParFiliereEtNiveau(Long idFiliere, Long idNveau){
		
		List<EtudiantAcademie> etudiants= null ;
		
		if(idFiliere != null && idNveau != null) {
			Query q = em.createQuery("select ets from EtudiantAcademie ets where ets.filiere.id=?1 "
			 		+ "and ets.niveau.id=?2 and ets.isActif=true") ;
			  q.setParameter(1, idFiliere) ;
			  q.setParameter(2, idNveau) ;
			   etudiants = q.getResultList() ;  
			  
		}
		
		return etudiants ;
		 
	}

	public EtudiantAcademie modifierAcademieEtudiant(EtudiantAcademie etudiant) {
		// TODO Auto-generated method stub
		EtudiantAcademie etudiantModif = rechercherEtudiantAcademieParId(etudiant.getId());
		etudiantModif = etudiant;
		em.merge(etudiantModif);

		return etudiantModif;
	}

	public EtudiantAcademie supprimerAcademieEtudiant(Long id) {
		
		EtudiantAcademie etudiant = rechercherEtudiantAcademieParId(id);
		etudiant.setActif(false);
		
		return etudiant;
		
	}

	public EtudiantAcademie rechercherEtudiantAcademieParId(Long id) {
		// TODO Auto-generated method stub
		
		EtudiantAcademie etudiant = em.find(EtudiantAcademie.class, id);
		return etudiant;
	}

	public EtudiantAcademie ajouter(EtudiantAcademie etudiant) {
		// TODO Auto-generated method stub
		etudiant.setActif(true);
		etudiant.setDateCreation(new Date());
		em.persist(etudiant);
		
		return etudiant ;
	}


}
