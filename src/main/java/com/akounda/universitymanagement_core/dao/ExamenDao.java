package com.akounda.universitymanagement_core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.IExamenDao;
import com.akounda.universitymanagement_core.domain.Examen;

@Repository
public class ExamenDao implements IExamenDao {

	@PersistenceContext
	private EntityManager em ;
	
	
	public Examen ajouter(Examen examen) {
		// TODO Auto-generated method stub
		em.persist(examen);
		return examen;
	}

	public List<Examen> recupererExamen() {
		// TODO Auto-generated method stub
		
		Query q = em.createQuery("select ex from Examen ex");
		List<Examen> examens = q.getResultList() ;
		return examens;
	}

	public Examen modfierExamen(Examen examen) {
		// TODO Auto-generated method stub
		em.merge(examen) ;
		return examen;
	}

	
}
