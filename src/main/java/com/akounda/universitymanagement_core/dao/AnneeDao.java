package com.akounda.universitymanagement_core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.IAnneeDao;
import com.akounda.universitymanagement_core.domain.Annee;

@Repository
public class AnneeDao implements IAnneeDao {

	@PersistenceContext
	private EntityManager em ;
	
	public Annee ajouter(Annee annee) {
		// TODO Auto-generated method stub
		 em.persist(annee);
		 
		 return annee ;
		
	}

	public List<Annee> recupererAnnees() {
		// TODO Auto-generated method stub
		
		javax.persistence.Query q = em.createQuery("select a from Annee a ") ;
		
		List<Annee> annees = q.getResultList() ;
		
		return annees ;
	}

	public Annee recupererParId(Long id) {
		// TODO Auto-generated method stub
		
		Annee annee = em.find(Annee.class, id) ;
		return annee;
	}

	public Annee modfierAnnee(Annee annee) {
		// TODO Auto-generated method stub
		
		em.merge(annee) ;
		return annee;
	}

}
