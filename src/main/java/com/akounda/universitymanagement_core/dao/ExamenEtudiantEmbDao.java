package com.akounda.universitymanagement_core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.akounda.universitymanagement_core.dao.interfaces.IExamenEtudiantEmbDao;
import com.akounda.universitymanagement_core.domain.ExamenEtudiantEmb;

@Repository
public class ExamenEtudiantEmbDao implements IExamenEtudiantEmbDao {
	
	@PersistenceContext
	private EntityManager entityManager ;

	public ExamenEtudiantEmb ajouter(ExamenEtudiantEmb examenEtudiantEmb) {
		// TODO Auto-generated method stub
		entityManager.persist(examenEtudiantEmb);
		
		return examenEtudiantEmb ;
	}

	public List<ExamenEtudiantEmb> recuperer() {
		// TODO Auto-generated method stub
		
		Query q = entityManager.createQuery("select e from EtudiantExamenEmb e") ;
		List<ExamenEtudiantEmb> embs = q.getResultList() ;
		
		return embs ;
	}

	public List<ExamenEtudiantEmb> recupererParExamen(Long idExamen) {
		// TODO Auto-generated method stub
		
		Query q = entityManager.createQuery("select e from ExamenEtudiantEmb e where e.examen.id =?1") ;
		q.setParameter(1, idExamen) ;
		List<ExamenEtudiantEmb> embs = q.getResultList() ;
		
		return embs ;
	}

}
