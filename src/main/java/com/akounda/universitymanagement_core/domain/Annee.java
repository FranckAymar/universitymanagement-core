package com.akounda.universitymanagement_core.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Annee {

	@Id
	@GeneratedValue
	private Long id ;
	private int anneeDebut ;
	private int anneeFin;
	
	
	public Annee(int anneeDebut, int anneeFin) {
		super();
		this.anneeDebut = anneeDebut;
		this.anneeFin = anneeFin;
	}



	public Annee() {
		super();
	}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getAnneeDebut() {
		return anneeDebut;
	}

	public void setAnneeDebut(int anneeDebut) {
		this.anneeDebut = anneeDebut;
	}

	public int getAnneeFin() {
		return anneeFin;
	}

	public void setAnneeFin(int anneeFin) {
		this.anneeFin = anneeFin;
	}

	
	
}
