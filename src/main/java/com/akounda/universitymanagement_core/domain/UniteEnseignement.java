package com.akounda.universitymanagement_core.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class UniteEnseignement {

	@Id
	@GeneratedValue
	private Long id ;
	private String libelle ;
	@ManyToOne()
	private Niveau niveau ;
	@ManyToOne()
	private Filiere filiere ;
	@ManyToOne
	private Professeur professeur ;
	@ManyToOne
	private Departement departement ;

	
	
	public UniteEnseignement(String libelle, Niveau niveau, Filiere filiere, Professeur professeur,Departement departement) {
		super();
		this.libelle = libelle;
		this.niveau = niveau;
		this.filiere = filiere;
		this.professeur = professeur ;
		this.departement = departement ;
	}
	
	
	
	public Professeur getProfesseur() {
		return professeur;
	}



	public void setProfesseur(Professeur professeur) {
		this.professeur = professeur;
	}



	public UniteEnseignement() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Niveau getNiveau() {
		return niveau;
	}
	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}
	public Filiere getFiliere() {
		return filiere;
	}
	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}



	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	
	
	
	
}
