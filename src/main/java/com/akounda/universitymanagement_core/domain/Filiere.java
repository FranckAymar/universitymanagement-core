package com.akounda.universitymanagement_core.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Filiere {

	@Id
	@GeneratedValue
	private long id ;
	private String libelle ;
	
	
	
	public Filiere() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Filiere(String libelle) {
		super();
		this.libelle = libelle;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	
	
	
	
	
	
}
