package com.akounda.universitymanagement_core.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.akounda.universitymanagement_core.domain.embeded.ExamenEtudiantId;

@Entity
public class ExamenEtudiantEmb implements  Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ExamenEtudiantId examenEtudiantId = new ExamenEtudiantId() ;
	
	@ManyToOne
	@JoinColumn(name = "examen_id", insertable = false, updatable = false)
	private Examen examen ;
	@ManyToOne
	@JoinColumn(name = 	"etudiantAcademie_id", insertable = false, updatable = false)
	private EtudiantAcademie etudiantAcademie ;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Note note ;
	
	

	public ExamenEtudiantEmb() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExamenEtudiantEmb(Examen examen , EtudiantAcademie etudiantAcademie, Note note) {
		
		this.examenEtudiantId.setIdExamen(examen.getId());
		this.examenEtudiantId.setIdEtudiantAcademie(etudiantAcademie.getId());
		
		this.examen = examen ;
		this.etudiantAcademie = etudiantAcademie ;
		this.note = note ;
	}
	
	public ExamenEtudiantId getExamenEtudiantId() {
		return examenEtudiantId;
	}

	public void setExamenEtudiantId(ExamenEtudiantId examenEtudiantId) {
		this.examenEtudiantId = examenEtudiantId;
	}

	public Examen getExamen() {
		return examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

	public Note getNote() {
		return note;
	}

	public void setNote(Note note) {
		this.note = note;
	}

	public EtudiantAcademie getEtudiantAcademie() {
		return etudiantAcademie;
	}

	public void setEtudiantAcademie(EtudiantAcademie etudiantAcademie) {
		this.etudiantAcademie = etudiantAcademie;
	}
	
	
	
	
	
}
