package com.akounda.universitymanagement_core.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Note {

	@Id
	@GeneratedValue
	private Long id ;
	private int note ;
	private String mention ;
	private String observation ;
	
	
	
	public Note(int note, String mention, String observation) {
		super();
		this.note = note;
		this.mention = mention;
		this.observation = observation;
	}
	public Note() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	public String getMention() {
		return mention;
	}
	public void setMention(String mention) {
		this.mention = mention;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	
	
	
	
}
