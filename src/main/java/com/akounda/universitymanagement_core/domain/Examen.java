package com.akounda.universitymanagement_core.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class Examen {
	
	@Id
	@GeneratedValue
	private Long id ;
	@ManyToOne()
	private UniteEnseignement uniteEnseignement ;
	private int session ;
	@Temporal(TemporalType.DATE)
	private java.util.Date date ;
	
	public Examen(UniteEnseignement uniteEnseignement , int session) {
		super();
		this.session = session;
		this.date = new java.util.Date();
		this.uniteEnseignement = uniteEnseignement ;
	}

	public Examen() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public UniteEnseignement getUe() {
		return uniteEnseignement;
	}

	public void setUe(UniteEnseignement ue) {
		this.uniteEnseignement = ue;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public int getSession() {
		return session;
	}

	public void setSession(int session) {
		this.session = session;
	}

	public java.util.Date getDate() {
		return date;
	}

	public void setDate(java.util.Date date) {
		this.date = date;
	}
	


}
