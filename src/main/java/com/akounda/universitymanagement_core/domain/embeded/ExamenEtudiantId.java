package com.akounda.universitymanagement_core.domain.embeded;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class ExamenEtudiantId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "examen_id")
	private Long idExamen ;
	@Column(name = "etudiantAcademie_id")
	private Long  idEtudiantAcademie;
	public Long getIdExamen() {
		return idExamen;
	}
	public void setIdExamen(Long idExamen) {
		this.idExamen = idExamen;
	}
	public Long getIdEtudiantAcademie() {
		return idEtudiantAcademie;
	}
	public void setIdEtudiantAcademie(Long idEtudiantAcademie) {
		this.idEtudiantAcademie = idEtudiantAcademie;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEtudiantAcademie == null) ? 0 : idEtudiantAcademie.hashCode());
		result = prime * result + ((idExamen == null) ? 0 : idExamen.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamenEtudiantId other = (ExamenEtudiantId) obj;
		if (idEtudiantAcademie == null) {
			if (other.idEtudiantAcademie != null)
				return false;
		} else if (!idEtudiantAcademie.equals(other.idEtudiantAcademie))
			return false;
		if (idExamen == null) {
			if (other.idExamen != null)
				return false;
		} else if (!idExamen.equals(other.idExamen))
			return false;
		return true;
	}
	
	
	
	
	
}
