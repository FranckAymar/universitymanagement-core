package com.akounda.universitymanagement_core.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class EtudiantAcademie {
	
	@Id
	@GeneratedValue
	private Long id;
	@ManyToOne
	private Annee annee ;
	@ManyToOne
	private Etudiant etudiant ;
	@ManyToOne
	private Departement departement ;
	@ManyToOne
	private Niveau niveau;
	@ManyToOne
	private Filiere filiere ;
	@Temporal(TemporalType.DATE)
	private Date dateCreation ;
	private String status ;
	private boolean isActif ;

	

	public EtudiantAcademie( Annee annee, Etudiant etudiant, Departement departement, Niveau niveau,
			Filiere filiere, String status) {
		super();
		this.annee = annee;
		this.etudiant = etudiant;
		this.departement = departement;
		this.niveau = niveau;
		this.filiere = filiere;
		this.dateCreation = new Date();
		this.status = status;
		this.isActif = true ;
		
	}

	public EtudiantAcademie() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}
	

	public void setId(Long id) {
		this.id = id;
	}

	public Annee getAnnee() {
		return annee;
	}

	public void setAnnee(Annee annee) {
		this.annee = annee;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public Niveau getNiveau() {
		return niveau;
	}

	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isActif() {
		return isActif;
	}

	public void setActif(boolean isActif) {
		this.isActif = isActif;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	
	
	
}
