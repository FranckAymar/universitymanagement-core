package com.akounda.universitymanagement_core.domain;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class Etudiant extends Personne  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isActif ;
	@Temporal(TemporalType.DATE)
	private Date dateCreation ;
	
	
	
	public Etudiant() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	


	public Etudiant(String nom, String prenom,String email) {
		super(nom,prenom, email);
		this.dateCreation = new Date() ;
		this.isActif = true ;
		
	}




	public boolean isActif() {
		return isActif;
	}




	public void setActif(boolean isActif) {
		this.isActif = isActif;
	}




	public Date getDateCreation() {
		return dateCreation;
	}




	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	

	
}
