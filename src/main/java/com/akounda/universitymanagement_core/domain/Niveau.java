package com.akounda.universitymanagement_core.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Niveau {

	@Id
	@GeneratedValue
	private Long id ;
	private String libelle ;
	
	
	
	public Niveau() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Niveau(String libelle) {
		super();
		this.libelle = libelle;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
}
