package com.akounda.universitymanagement_core.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity 
@PrimaryKeyJoinColumn(name = "id")
public class Professeur extends Personne {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String specialite ;
	private String laboratoire ;
	@Temporal(TemporalType.DATE)
	private Date dateCreation ;
	
	private boolean isActif ;

	
	public Professeur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	


	public Professeur(String nom, String prenom, String specialité, String laboratoire, String email) {
		super(nom, prenom, email);
		this.specialite = specialité;
		this.laboratoire = laboratoire;
		this.dateCreation = new Date()  ;
		this.isActif = true ;
	}




	public String getSpecialite() {
		return specialite;
	}




	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}




	public String getLaboratoire() {
		return laboratoire;
	}




	public void setLaboratoire(String laboratoire) {
		this.laboratoire = laboratoire;
	}




	public Date getDateCreation() {
		return dateCreation;
	}




	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}




	public boolean isActif() {
		return isActif;
	}




	public void setActif(boolean isActif) {
		this.isActif = isActif;
	}


	

	
}
