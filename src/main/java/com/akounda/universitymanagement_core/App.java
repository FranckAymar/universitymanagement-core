package com.akounda.universitymanagement_core;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.transaction.Transactional;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.akounda.universitymanagement_core.config.AppConfig;
import com.akounda.universitymanagement_core.domain.Annee;
import com.akounda.universitymanagement_core.domain.Etudiant;
import com.akounda.universitymanagement_core.domain.Filiere;
import com.akounda.universitymanagement_core.domain.Niveau;
import com.akounda.universitymanagement_core.domain.Professeur;
import com.akounda.universitymanagement_core.domain.User;
import com.akounda.universitymanagement_core.service.ProfesseurService;
import com.akounda.universitymanagement_core.service.interfaces.IAnneService;
import com.akounda.universitymanagement_core.service.interfaces.IEtudiantService;
import com.akounda.universitymanagement_core.service.interfaces.IProfesseurService;

/**
 * Hello world!
 *
 */
public class App 
{
	
    public static void main( String[] args )
    {
    	
    	AnnotationConfigApplicationContext context = 
    			new AnnotationConfigApplicationContext(AppConfig.class) ;
    	
    	IEtudiantService etudiantService = context.getBean(IEtudiantService.class);
    	IProfesseurService professeurService = context.getBean(IProfesseurService.class) ;
    	IAnneService anneService = context.getBean(IAnneService.class);
		
    	/*		
    	Etudiant etudiant = new Etudiant() ;
    	etudiant.setNom("Braffouo");
    	etudiant.setNiveau(new Niveau("Licence 1"));
    	etudiant.setFiliere(new Filiere("EEAI"));
    	Annee annee = new Annee() ;
    	annee.setLibelle("2019");
    	
    	Annee annee2 = new Annee() ;
    	annee.setLibelle("2020");
 		
    	List<Annee> annees = new ArrayList<Annee>() ;
    	annees.add(annee) ;    	
    	annees.add(annee2) ;
    	
    	//etudiant.setAnnees(annees); 
    	
    	etudiantService.ajouter(etudiant) ;
    
    	
		 context.close();
    	
        
    	IBouteilleVenteService bvService = context.getBean(IBouteilleVenteService.class);

        	
        	//Ajouter
        	
        	BouteilleVenteEmb bouteilleVenteEmb = bvService.rechercherParIdEmb(10L, 134L) ;
        	System.out.println(bouteilleVenteEmb.getQuantite());
    	
    	context.close();
    	
    	
		/*
		 * EntityManagerFactory entityManagerFactory =
		 * Persistence.createEntityManagerFactory( "persistenceUnitHibernate" );
		 * 
		 * EntityManager entityManager = entityManagerFactory.createEntityManager();
		 * 
		 * EntityTransaction tr = entityManager.getTransaction() ;
		 * 
		 * tr.begin();
		 * 
		 * User user = new User("Braffouo", "Franck"); Etudiant etudiant = new
		 * Etudiant(); etudiant.setNom("Traore"); etudiant.setPrenom("Kikoun");
		 * etudiant.setNiveau(1); etudiant.setFiliere("EEAI");
		 * 
		 * Professeur professeur = new Professeur() ; professeur.setNom("Ouattara ");
		 * professeur.setPrenom("Lacine"); professeur.setSpecialité("CHIMIE");
		 * professeur.setLaboratoire("LSS");
		 * 
		 * entityManager.persist(user); entityManager.persist(professeur);
		 * entityManager.persist(etudiant);
		 * 
		 * tr.commit();
		 * 
		 */
   
    	
    }
    
    public static void afficher (List<Etudiant> etds) {
    	 
		 for (Etudiant etudiant2 : etds) {
			//System.out.println(etudiant2.getNom() + "-"+ etudiant2.getAnnees());
		}
    }
    
    
    public static void afficherProf (List<Professeur> profs) {
    	 
		 for (Professeur etudiant2 : profs) {
			System.out.println(etudiant2.getNom());
		}
    }
}
