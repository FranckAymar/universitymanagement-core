package com.akounda.universitymanagement_core.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.IProfesseurDao;
import com.akounda.universitymanagement_core.domain.Professeur;
import com.akounda.universitymanagement_core.service.interfaces.IProfesseurService;

@Service
public class ProfesseurService implements IProfesseurService {

	@Autowired
	private IProfesseurDao profDao ;

	@Transactional
	public Professeur ajouter(Professeur prof) {
		// TODO Auto-generated method stub
		
		return profDao.ajouter(prof);
	}

	public List<Professeur> recupererPro() {
		// TODO Auto-generated method stub
		return profDao.recupererPro();
	}

	@Transactional
	public Professeur modifierPro(Professeur prof) {
		// TODO Auto-generated method stub
		return profDao.modifierPro(prof);
	}

	public Professeur supprimerPro(Long id) {
		// TODO Auto-generated method stub
		return profDao.supprimerPro(id);
	}

	@Transactional
	public Professeur recupererProParId(Long id) {
		// TODO Auto-generated method stub
		return profDao.recupererProParId(id);
	}
	
	
}
