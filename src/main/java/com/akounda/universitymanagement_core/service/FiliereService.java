package com.akounda.universitymanagement_core.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.IFiliereDao;
import com.akounda.universitymanagement_core.domain.Filiere;
import com.akounda.universitymanagement_core.service.interfaces.IFiliereService;

@Service
@Transactional
public class FiliereService implements IFiliereService {

	@Autowired
	private IFiliereDao filiereDao ;
	
	public Filiere ajouter(Filiere filiere) {
		// TODO Auto-generated method stub
		return filiereDao.ajouter(filiere);
	}

	public List<Filiere> recupererFiliere() {
		// TODO Auto-generated method stub
		return filiereDao.recupererFiliere();
	}

	public Filiere rechercherFiliereParId(Long id) {
		// TODO Auto-generated method stub
		return filiereDao.rechercherFiliereParId(id);
	}

	public Filiere supprimerFiliere(Long id) {
		// TODO Auto-generated method stub
		return filiereDao.supprimerFiliere(id);
	}

	public Filiere modifierFiliere(Filiere filiere) {
		// TODO Auto-generated method stub
		return filiereDao.modifierFiliere(filiere);
	}

}
