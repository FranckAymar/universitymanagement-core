package com.akounda.universitymanagement_core.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.IAnneeDao;
import com.akounda.universitymanagement_core.domain.Annee;
import com.akounda.universitymanagement_core.service.interfaces.IAnneService;

@Service

public class AnneeService implements IAnneService {
	
	@Autowired
	private IAnneeDao aDao ;

	@Transactional
	public Annee ajouter(Annee annee) {
		// TODO Auto-generated method stub
		return aDao.ajouter(annee);
	}

	@Transactional
	public List<Annee> recupererAnnees() {
		// TODO Auto-generated method stub
		return aDao.recupererAnnees();
	}

	public Annee recupererParId(Long id) {
		// TODO Auto-generated method stub
		return aDao.recupererParId(id);
	}

	@Transactional
	public Annee modfierAnnee(Annee annee) {
		// TODO Auto-generated method stub
		return aDao.modfierAnnee(annee);
	}
	
	

}
