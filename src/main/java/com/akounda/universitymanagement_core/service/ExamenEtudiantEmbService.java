package com.akounda.universitymanagement_core.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.IEtudiantAcademieDao;
import com.akounda.universitymanagement_core.dao.interfaces.IExamenEtudiantEmbDao;
import com.akounda.universitymanagement_core.domain.EtudiantAcademie;
import com.akounda.universitymanagement_core.domain.Examen;
import com.akounda.universitymanagement_core.domain.ExamenEtudiantEmb;
import com.akounda.universitymanagement_core.domain.Note;
import com.akounda.universitymanagement_core.model.EtudiantNote;
import com.akounda.universitymanagement_core.service.interfaces.IExamenEtudiantEmbService;

@Service
@Transactional
public class ExamenEtudiantEmbService implements IExamenEtudiantEmbService {

	@Autowired
	private IExamenEtudiantEmbDao examenEtudiantEmbDao;
	
	@Autowired
	private IEtudiantAcademieDao etudiantAcademieDao ;

	public ExamenEtudiantEmb ajouter(ExamenEtudiantEmb examenEtudiantEmb) {
		// TODO Auto-generated method stub
		return examenEtudiantEmbDao.ajouter(examenEtudiantEmb);
	}

	public List<ExamenEtudiantEmb> recuperer() {
		// TODO Auto-generated method stub
		return examenEtudiantEmbDao.recuperer();
	}

	public List<ExamenEtudiantEmb> recupererParExamen(Long idExamen) {
		// TODO Auto-generated method stub
		return examenEtudiantEmbDao.recupererParExamen(idExamen);
	}

	public List<ExamenEtudiantEmb> assignerExamenEtudiants(Examen examen, Map<Long, Integer> etudiantNotesArray) {
			
			
			EtudiantAcademie etudiantAcademie;
			ExamenEtudiantEmb examenEtudiantEmb ;
			Note note ;
		
			List<ExamenEtudiantEmb> examenEtudiantEmbs = new ArrayList<ExamenEtudiantEmb>()  ;
			
			Iterator etudiantNoteArrayIterator = etudiantNotesArray.entrySet().iterator()  ;
				
					while (etudiantNoteArrayIterator.hasNext()) {
					
						Map.Entry<Long, Integer> mapEntry = (Map.Entry<Long, Integer>) etudiantNoteArrayIterator.next() ;
						etudiantAcademie = etudiantAcademieDao.rechercherEtudiantAcademieParId(mapEntry.getKey()) ;
						note = new Note(mapEntry.getValue(), null, null) ;
						
						examenEtudiantEmb = new ExamenEtudiantEmb(examen, etudiantAcademie, note) ;
						examenEtudiantEmbDao.ajouter(examenEtudiantEmb) ;
						examenEtudiantEmbs.add(examenEtudiantEmb) ;
					}
				
				
				
				
			
			
			return examenEtudiantEmbs ;
		}
	



}
