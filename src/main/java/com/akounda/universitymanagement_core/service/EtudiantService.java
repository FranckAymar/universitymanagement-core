package com.akounda.universitymanagement_core.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.IEtudiantDao;
import com.akounda.universitymanagement_core.domain.Etudiant;
import com.akounda.universitymanagement_core.service.interfaces.IEtudiantService;

@Service
public class EtudiantService implements IEtudiantService {


	@Autowired
	private IEtudiantDao etudiantDao ;
	
	@Transactional
	public Etudiant ajouter(Etudiant etudiant) {
		// TODO Auto-generated method stub
		return etudiantDao.ajouter(etudiant);
	}

	public List<Etudiant> getAllEtudiant() {
		// TODO Auto-generated method stub
		return etudiantDao.getAllEtudiant();
	}

	@Transactional
	public Etudiant modifier(Etudiant etudiant) {
		// TODO Auto-generated method stub
		return etudiantDao.modifier(etudiant);
	}

	@Transactional
	public Etudiant supprimer(Long id) {
		// TODO Auto-generated method stub
		return etudiantDao.supprimer(id);
	}

	public Etudiant rechercherEtudiantParId(Long id) {
		// TODO Auto-generated method stub
		return etudiantDao.rechercherEtudiantParId(id);
	}

}
