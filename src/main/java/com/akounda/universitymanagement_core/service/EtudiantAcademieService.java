package com.akounda.universitymanagement_core.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.IEtudiantAcademieDao;
import com.akounda.universitymanagement_core.domain.EtudiantAcademie;
import com.akounda.universitymanagement_core.service.interfaces.IEtudiantAcademieService;

@Service
@Transactional
public class EtudiantAcademieService implements IEtudiantAcademieService {
	
	@Autowired
	private IEtudiantAcademieDao etudiantAcademieDao ;

	public EtudiantAcademie ajouter(EtudiantAcademie etudiant) {
		// TODO Auto-generated method stub
		return etudiantAcademieDao.ajouter(etudiant);
	}
 
	public List<EtudiantAcademie> getAllEtudiantAcademie(Long idAnnee) {
		// TODO Auto-generated method stub
		return etudiantAcademieDao.getAllEtudiantAcademie(idAnnee);
	}

	public EtudiantAcademie modifierAcademieEtudiant(EtudiantAcademie etudiant) {
		// TODO Auto-generated method stub
		return etudiantAcademieDao.modifierAcademieEtudiant(etudiant);
	}

	public EtudiantAcademie supprimerAcademieEtudiant(Long id) {
		// TODO Auto-generated method stub
		return etudiantAcademieDao.supprimerAcademieEtudiant(id);
	}

	public EtudiantAcademie rechercherEtudiantAcademieParId(Long id) {
		// TODO Auto-generated method stub
		return etudiantAcademieDao.rechercherEtudiantAcademieParId(id);
	}

	public List<EtudiantAcademie> recupererParNiveauFiliere(Long idFiliere, Long idNiveau) {
		// TODO Auto-generated method stub
		return etudiantAcademieDao.recupererParFiliereEtNiveau(idFiliere, idNiveau);
	}
  
}
