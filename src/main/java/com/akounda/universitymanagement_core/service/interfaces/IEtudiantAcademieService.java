package com.akounda.universitymanagement_core.service.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.EtudiantAcademie;

public interface IEtudiantAcademieService {
	
	public EtudiantAcademie ajouter(EtudiantAcademie etudiant);

	public List<EtudiantAcademie> getAllEtudiantAcademie(Long idAnnee) ;
	
	public List<EtudiantAcademie> recupererParNiveauFiliere(Long idFiliere, Long idNiveau) ;

	public EtudiantAcademie modifierAcademieEtudiant(EtudiantAcademie etudiant) ;
	
	public EtudiantAcademie supprimerAcademieEtudiant(Long id) ;
	
	public EtudiantAcademie rechercherEtudiantAcademieParId(Long id) ;

}
