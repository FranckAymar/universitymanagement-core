package com.akounda.universitymanagement_core.service.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Examen;
import com.akounda.universitymanagement_core.domain.UniteEnseignement;
import com.akounda.universitymanagement_core.model.EtudiantNote;

public interface IExamenService {

	
	public Examen ajouter(Examen examen) ;
	public List<Examen> recupererExamen() ;
	public Examen modfierExamen(Examen examen) ;
}
