package com.akounda.universitymanagement_core.service.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Professeur;

public interface IProfesseurService {

	public Professeur ajouter(Professeur prof ) ;
	public List<Professeur> recupererPro() ;
	public Professeur modifierPro(Professeur prof) ;
	public Professeur supprimerPro(Long id) ;
	public Professeur recupererProParId(Long id) ;
}
