package com.akounda.universitymanagement_core.service.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Etudiant;

public interface IEtudiantService {
	
	public Etudiant ajouter(Etudiant etudiant);
	public List<Etudiant> getAllEtudiant() ;
	public Etudiant modifier(Etudiant etudiant) ;
	public Etudiant supprimer(Long id) ;
	public Etudiant rechercherEtudiantParId(Long id) ;



}
