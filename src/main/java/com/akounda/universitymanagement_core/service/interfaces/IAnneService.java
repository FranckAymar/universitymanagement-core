package com.akounda.universitymanagement_core.service.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Annee;

public interface IAnneService {

	public Annee ajouter(Annee annee) ;
	public List<Annee> recupererAnnees() ;
	public Annee recupererParId(Long id) ;
	public Annee modfierAnnee(Annee annee) ;

}
