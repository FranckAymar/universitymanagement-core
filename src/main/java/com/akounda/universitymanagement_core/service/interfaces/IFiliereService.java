package com.akounda.universitymanagement_core.service.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Filiere;

public interface IFiliereService {

	
	public Filiere ajouter(Filiere filiere) ;
	public List<Filiere> recupererFiliere() ;
	public Filiere rechercherFiliereParId(Long id);
	public Filiere supprimerFiliere(Long id) ;
	public Filiere modifierFiliere(Filiere filiere) ;
	
	
}
