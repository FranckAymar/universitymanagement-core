package com.akounda.universitymanagement_core.service.interfaces;

import java.util.List;

import com.akounda.universitymanagement_core.domain.Departement;

public interface IDepartementService {
	
	public Departement ajouter(Departement departement) ;
	public List<Departement> recupererDepartement() ;
	public Departement rechercherDepartementParId(Long id);
	public Departement supprimerDepartement(Long id) ;
	public Departement modifierDepartement(Departement departement) ;

}
