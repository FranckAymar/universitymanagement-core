package com.akounda.universitymanagement_core.service.interfaces;

import java.util.List;
import java.util.Map;

import com.akounda.universitymanagement_core.domain.Examen;
import com.akounda.universitymanagement_core.domain.ExamenEtudiantEmb;

public interface IExamenEtudiantEmbService {
	
	public ExamenEtudiantEmb ajouter(ExamenEtudiantEmb examenEtudiantEmb) ;
	public List<ExamenEtudiantEmb> recuperer() ;
	public List<ExamenEtudiantEmb> recupererParExamen(Long idExamen) ;
	public List<ExamenEtudiantEmb> assignerExamenEtudiants(Examen examen, Map<Long, Integer> etudiantNotesArray);
	

}
