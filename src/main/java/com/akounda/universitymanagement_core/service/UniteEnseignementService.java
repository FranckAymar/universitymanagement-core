package com.akounda.universitymanagement_core.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.IUniteEnseignementDao;
import com.akounda.universitymanagement_core.domain.UniteEnseignement;
import com.akounda.universitymanagement_core.service.interfaces.IUniteEnseignementService;

@Service
@Transactional
public class UniteEnseignementService implements IUniteEnseignementService {

	@Autowired
	private IUniteEnseignementDao enseignementDao ;
	
	
	public UniteEnseignement recupererUE(Long id) {
		// TODO Auto-generated method stub
		return enseignementDao.recupererUE(id);
	}
	public UniteEnseignement ajouter(UniteEnseignement ue) {
		// TODO Auto-generated method stub
		return enseignementDao.ajouter(ue);
	}
	public List<UniteEnseignement> recupererUEs() {
		// TODO Auto-generated method stub
		return enseignementDao.recupererUEs();
	}
	public UniteEnseignement modifier(UniteEnseignement ue) {
		// TODO Auto-generated method stub
		return enseignementDao.modifier(ue);
	}

}
