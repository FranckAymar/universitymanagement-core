package com.akounda.universitymanagement_core.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.IDepartementDao;
import com.akounda.universitymanagement_core.domain.Departement;
import com.akounda.universitymanagement_core.service.interfaces.IDepartementService;

@Service
@Transactional
public class DepartementService implements IDepartementService {
	
	@Autowired
	private IDepartementDao departementDao ;

	public Departement ajouter(Departement departement) {
		// TODO Auto-generated method stub
		return departementDao.ajouter(departement);
	}

	public List<Departement> recupererDepartement() {
		// TODO Auto-generated method stub
		return departementDao.recupererDepartement();
	}

	public Departement rechercherDepartementParId(Long id) {
		// TODO Auto-generated method stub
		return departementDao.rechercherDepartementParId(id);
	}

	public Departement supprimerDepartement(Long id) {
		// TODO Auto-generated method stub
		return departementDao.supprimerDepartement(id);
	}

	public Departement modifierDepartement(Departement departement) {
		// TODO Auto-generated method stub
		return departementDao.modifierDepartement(departement);
	}

}
