package com.akounda.universitymanagement_core.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.INiveauDao;
import com.akounda.universitymanagement_core.domain.Niveau;
import com.akounda.universitymanagement_core.service.interfaces.INiveauService;

@Service
@Transactional
public class NiveauService implements INiveauService {

	@Autowired
	private INiveauDao niveauDao ;
	
	public Niveau ajouter(Niveau niveau) {
		// TODO Auto-generated method stub
		return niveauDao.ajouter(niveau);
	}

	public List<Niveau> recupererNiveaux() {
		// TODO Auto-generated method stub
		return niveauDao.recupererNiveaux();
	}

	public Niveau rechercherNiveauParId(Long id) {
		// TODO Auto-generated method stub
		return niveauDao.rechercherNiveauParId(id);
	}

	public Niveau supprimerNiveau(Long id) {
		// TODO Auto-generated method stub
		return niveauDao.supprimerNiveau(id);
	}

	public Niveau modifierNiveau(Niveau niveau) {
		// TODO Auto-generated method stub
		return niveauDao.modifierNiveau(niveau);
	}

}
