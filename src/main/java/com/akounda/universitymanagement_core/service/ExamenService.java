package com.akounda.universitymanagement_core.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akounda.universitymanagement_core.dao.interfaces.IExamenDao;
import com.akounda.universitymanagement_core.domain.EtudiantAcademie;
import com.akounda.universitymanagement_core.domain.Examen;
import com.akounda.universitymanagement_core.domain.Note;
import com.akounda.universitymanagement_core.domain.UniteEnseignement;
import com.akounda.universitymanagement_core.model.EtudiantNote;
import com.akounda.universitymanagement_core.service.interfaces.IExamenService;

@Service
@Transactional
public class ExamenService implements IExamenService {

	@Autowired
	private IExamenDao examenDao ;
	
	
	public Examen ajouter(Examen examen) {
		// TODO Auto-generated method stub
		return examenDao.ajouter(examen);
	}

	public List<Examen> recupererExamen() {
		// TODO Auto-generated method stub
		return examenDao.recupererExamen(); 
	}

	public Examen modfierExamen(Examen examen) {
		// TODO Auto-generated method stub
		return examenDao.modfierExamen(examen);
	}
	
	
}
